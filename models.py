"""
Classes to encapsulate data sets, models, and training pipelines.
"""

import logging

import pandas as pd
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from tqdm import tqdm


logging.basicConfig(format='%(levelname)s : %(asctime)s : %(message)s', level=logging.INFO)
log = logging.getLogger()


class DataSet:
    def __init__(self, name, df, target, test_size=0.15, scale_features=True, seed=0, verbose=True):
        self.name = name
        self.df = df
        self.target = target
        self.feature_df = self.df.drop(self.target, axis=1)
        self.test_size = test_size
        self.scale_features = scale_features
        self.scaler = StandardScaler()
        self.seed = seed
        self.verbose = verbose
        self._one_hot = OneHotEncoder()
        self._prep_data()
        
        # encode target
        
    def _prep_data(self):
        self.x_train, self.x_test, self.y_train, self.y_test = (
            train_test_split(
                self.feature_df, self.df[self.target], test_size=self.test_size, random_state=self.seed
            )
        )
        if self.scale_features:
            self.x_train = self.scaler.fit_transform(self.x_train)
            self.x_test = self.scaler.transform(self.x_test)
        if self.verbose:
            log.info(f'Data prepared.')
     
    @property
    def y_train_hot(self):
        return self._one_hot.fit_transform(
            self.y_train.to_numpy().reshape(-1, 1)).todense()
    
    @property
    def y_test_hot(self):
        return self._one_hot.fit_transform(
            self.y_test.to_numpy().reshape(-1, 1)).todense()
        
    
class Model:
    def __init__(self, name, estimator, params, estimator_args=None, verbose=True):
        self.name = name
        self.estimator = estimator
        self.params = params
        self.estimator_args = estimator_args
        self.verbose = verbose
        if self.estimator_args:
            self.clf = self.estimator(**self.estimator_args)
        else:
            self.clf = self.estimator()
        if self.verbose:
            log.info(f'Model {self.name} created.')
            
            
class TrainingPipeline:
    def __init__(self, model, dataset, training_subsamples=None, verbose=True):
        self.model = model
        self.dataset = dataset
        self.training_subsamples = training_subsamples if training_subsamples else (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)
        self.verbose = verbose
        self.searchers = {}
        
    def run(self):
        if self.verbose:
            log.info(f'Beginning pipeline with model {self.model.name} and dataset {self.dataset.name}.')
        for size in tqdm(self.training_sizes):
            searcher = GridSearchCV(self.model.clf, self.model.params, scoring='accuracy', return_train_score=True)
            searcher.fit(self.dataset.x_train[:size], self.dataset.y_train[:size])
            self.searchers[size] = searcher
        if self.verbose:
            log.info(f'Pipeline complete.')
        
    @property
    def results_df(self):
        dfs = []
        for size, searcher in self.searchers.items():
            df = pd.DataFrame(searcher.cv_results_)
            df['training_samples'] = size
            df['training_frac'] = df['training_samples'] / self.training_sizes[-1]
            dfs.append(df)
        return pd.concat(dfs)

    @property
    def training_sizes(self):
        total_size = self.dataset.x_train.shape[0]
        return [int(round(frac * total_size)) for frac in self.training_subsamples]