import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

def create_error_fig(pipes):
    fig = make_subplots(
        rows=1, cols=5,
        shared_xaxes=True, shared_yaxes=True,
        subplot_titles=list(pipes.keys()),
        x_title='Percent of Training Data'
    )
    for idx, (key, pipe) in enumerate(pipes.items()):
        show_legend = True if idx == 0 else False
        pipe = pipes[key]
        fig.add_trace(
            go.Scatter(
                x=pipe.results_df['training_frac'],
                y=pipe.results_df['mean_train_score'],
                line=dict(color= '#D55E00'),
                showlegend=show_legend,
                name='Train'
            ),
            row=1, col=idx+1,
        )
        fig.add_trace(
            go.Scatter(
                x=pipe.results_df['training_frac'],
                y=pipe.results_df['mean_test_score'],
                line=dict(
                    color='#0072B2',
                    dash='dash'
                ),
                showlegend=show_legend,
                name='Cross Val.'
            ),
            row=1, col=idx+1
        )
        fig.update_xaxes(tickformat='.0%', dtick=0.25, row=1, col=idx+1)
    fig.update_layout(yaxis=dict(title='Classification Accuracy', tickformat='.0%'))
    return fig


def create_time_fig(pipes):
    fig = make_subplots(
        rows=1, cols=5,
        shared_xaxes=True, shared_yaxes=True,
        subplot_titles=list(pipes.keys()),
        x_title='Percent of Training Data'
    )
    for idx, (key, pipe) in enumerate(pipes.items()):
        show_legend = True if idx == 0 else False
        pipe = pipes[key]
        fig.add_trace(
            go.Scatter(
                x=pipe.results_df['training_frac'],
                y=pipe.results_df['mean_fit_time'],
                line=dict(color= '#D55E00'),
                showlegend=show_legend,
                name='Fit'
            ),
            row=1, col=idx+1,
        )
        '''
        fig.add_trace(
            go.Scatter(
                x=pipe.results_df['training_frac'],
                y=pipe.results_df['mean_score_time'],
                line=dict(
                    color='#0072B2',
                    dash='dash'
                ),
                showlegend=show_legend,
                name='Predict'
            ),
            row=1, col=idx+1
        )
        '''
        fig.update_xaxes(tickformat='.0%', dtick=0.25, row=1, col=idx+1)
        fig.update_yaxes(type="log", row=1, col=idx+1)
    fig.update_layout(yaxis=dict(title='Time (seconds)'))
    return fig


def create_heatmap(df, x_col, y_col, z_col):
    x, y, z = _format_xyz(df, x_col, y_col, z_col)
    x_label = x_col.split('param_')[1] if 'param_' in x_col else x_col
    y_label = y_col.split('param_')[1] if 'param_' in y_col else y_col
    fig = go.Figure()
    fig.add_trace(
        go.Heatmap(
            z=z, x=x, y=y,
            colorscale='RdYlGn'
        )
    )
    fig.update_layout(
        autosize=False,
        width=500,
        height=500,
        xaxis = dict(
            title=x_label,
            tickmode = 'array',
            tickvals = x
        ),
        yaxis = dict(
            title=y_label,
            tickmode = 'array',
            tickvals = y
        )
    )
    return fig


def _format_xyz(df, x_col, y_col, z_col):
    x_vals = np.sort(df[x_col].unique())
    y_vals = np.sort(df[y_col].unique())
    z = []
    for y in y_vals:
        inner_z = []
        for x in x_vals:
            z_val = df[(df[x_col] == x) & (df[y_col] == y)].iloc[0][z_col]
            inner_z.append(z_val)
        z.append(inner_z)
    return x_vals, y_vals, z


def create_2_heatmaps(dfs, x_col, y_col, z_col, title, subplot_titles, x_title, y_title):
    fig = make_subplots(
        rows=1, cols=2,
        shared_xaxes=True, shared_yaxes=True,
        subplot_titles=subplot_titles,
        x_title=x_title,
        y_title=y_title
    )
    for idx, df in enumerate(dfs):
        x, y, z = _format_xyz(df, x_col=x_col, y_col=y_col, z_col=z_col)
        fig.add_trace(
            go.Heatmap(
                z=z, x=x, y=y,
                coloraxis = "coloraxis",
            ),
            row=1, col=idx+1
        )
    fig.update_layout(
        title={
            'text': title,
            'y':0.81,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        height=350, width=600, 
        coloraxis={'colorscale':'RdYlGn'},
        font_size=12
    )
    for anno in fig['layout']['annotations']:
        anno['font'] = {'size': 16}
        if anno['text'] == x_title:
            anno['yshift'] = -9
        if anno['text'] == y_title:
            anno['xshift'] = -25
    return fig